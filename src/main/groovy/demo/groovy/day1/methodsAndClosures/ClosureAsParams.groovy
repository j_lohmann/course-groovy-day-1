package demo.groovy.day1.methodsAndClosures

def callClosure(param, Closure closure) {
    println "--->"
    closure.call(param)
    println "<---"
}

def myClosure = { println it }


callClosure(" Hello world ", myClosure)

callClosure(" Hello world ", { println it.toUpperCase() })

callClosure(" Hello world ") {
    System.out.println it.reverse()
}
