package demo.groovy.day1.methodsAndClosures


def closure = { say, to, who ->
    println "$say $to $who"
}

closure("Hello", "student", " how are you ")

def hi = closure.curry("Hi")
hi("student", "how are you")

def whatsUp = closure.rcurry("what's up")
whatsUp("Yo", "dude")

def wazUpSir = closure.ncurry(1, "Sir", "waz up")
wazUpSir("G'day")

def yoWazup = whatsUp.curry("Yo")
yoWazup("bro")
