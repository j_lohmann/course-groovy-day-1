package demo.groovy.day1.methodsAndClosures


// Make this commands args and return value satisfy the asserts
def namedArgs() {

}

assert "Hello student, you're 43 years old." == namedArgs(name: 'student', 'Hello', age: 43)

assert "Hello student, you're 43 years old." == namedArgs(name: 'student', age: 43)

