package demo.groovy.day1.methodsAndClosures.answer


def namedArgs(Map params, String greeting = "Hello") {
    return "$greeting $params.name, you're $params.age years old." // some string
}

assert "Hello student, you're 43 years old." == namedArgs(name: 'student', 'Hello', age: 43)
assert "Hello student, you're 43 years old." == namedArgs(name: 'student', age: 43)
