package demo.groovy.day1

import groovy.transform.ToString

@ToString(includeNames = true)
class Person {
    String firstName
    String lastName
    int age
}
