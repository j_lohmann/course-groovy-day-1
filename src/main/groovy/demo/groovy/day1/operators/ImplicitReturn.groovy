package demo.groovy.day1.operators

def myMethod(int i) {
    if (i == 0) {
        "Zero"
    } else if (i > 0) {
        "Greater than zero"
    } else {
        "Less than zero"
    }
}

println myMethod(0)  // --> Zero
println myMethod(1)  // --> Greater than zero
println myMethod(-1) // --> Less than zero
