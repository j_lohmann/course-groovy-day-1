package demo.groovy.day1.operators.answer

def list = [[2,4],[1,4],[2,3],[1,1]]

def closure = { a, b ->
    a[0] <=> b[0] ?: a[1] <=> b[1]
}

assert list.sort(closure) == [[1,1],[1,4],[2,3],[2,4]]

