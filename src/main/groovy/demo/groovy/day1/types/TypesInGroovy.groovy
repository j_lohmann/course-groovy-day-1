package demo.groovy.day1.types

println(42.class.name)          // --> java.lang.Integer
println(1.2F.class.name)        // --> java.lang.Float
println(1.2D.class.name)        // --> java.lang.Double
println(1.2.class.name)         // --> java.math.BigDecimal
println(true.class.name)        // --> java.lang.Boolean
println(((char) 'G').class.name)// --> java.lang.Character
println("abc".class.name)       // --> java.lang.String
println('abc'.class.name)       // --> java.lang.String


//println("${1}".class.name)      // --> ?
