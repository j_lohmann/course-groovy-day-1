package demo.groovy.day1.types

def variable = 10
println(variable.class.name)    // -> java.lang.Integer
variable="Test"
println(variable.class.name)    // -> java.lang.String
variable= 10G
println(variable.class.name)    // -> java.math.BigInteger

variable=10.0
//println(variable.class.name)    // --> ?

//println 1.0F + 0.1F
//println 1.0 + 0.1
