package demo.groovy.day1.types

class Duck {
    String name

    void quack() {
        println "My name is $name and I go quack quack quack..."
    }
}

class Mouse {
    String name

    void quack() {
        println "My name is $name and I don't go quack quack quack..."
    }
}

def makeItGoQuack(def animal) {
    animal.quack()
}

def duck = new Duck(name: "Donald")
def mouse = new Mouse(name: "Micky")


makeItGoQuack(duck)
makeItGoQuack(mouse)
