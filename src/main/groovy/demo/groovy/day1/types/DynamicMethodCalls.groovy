package demo.groovy.day1.types

class Dog {
    void sitCommand() {
        println "Sittng down"
    }

    void barkCommand() {
        println "Woof woof..."
    }

    void standCommand() {
        println "Standing up"
    }
}

def dog = new Dog()

def command(Dog dog, String command) {
    dog."${command}Command"()
}

command(dog, "sit")
command(dog, "stand")
command(dog, "bark")
command(dog, "run")

