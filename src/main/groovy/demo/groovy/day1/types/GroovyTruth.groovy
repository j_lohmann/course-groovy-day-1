package demo.groovy.day1.types

import demo.groovy.day1.Person

class LegalPerson extends Person {
    boolean asBoolean() {
        age >= 18
    }
}


def minor = new LegalPerson(age: 14)
assert !minor

def adult = new LegalPerson(age: 44)
assert adult

