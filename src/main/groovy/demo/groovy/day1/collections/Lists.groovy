package demo.groovy.day1.collections

def empty = []
println empty.class.name // --> java.util.ArrayList

class User {
    String username
    String toString() { username }
}

def student = new User(username: 'student')

def items = ['Groovy', 12, student]
println items.size()   // --> 3
println items[-2]      // --> 12 (Read array backwards)
println items.reverse()// --> ['student', 12, 'Groovy ']

items << ' Grails'    // Add the left shift operator.
println items         // --> ['Groovy', 12, 'student', 'Grails']
println items + [1, 2]// --> ['Groovy', 12, 'student', 'Grails', 1, 2]
println items - [student, 12] // --> ['Groovy', 'Grails']

Set set = [100,2,1,1,3,4,5,5,6] as Set
println set           // --> ?
println set.class.name
