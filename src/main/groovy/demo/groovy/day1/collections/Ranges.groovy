package demo.groovy.day1.collections

// Simple range of ints
println 1..10

// Exclude last element
println 'a'..<'z'

println 'aa'..'aå'

// Enums can also be used in the ranges:
enum Compass {
    NORTH, NORTH_EAST,
    EAST, SOUTH_EAST,
    SOUTH, SOUTH_WEST,
    WEST, NORTH_WEST
}

println Compass.NORTH..Compass.SOUTH

println 1..-1
