package demo.groovy.day1.collections

def empty = []
println empty.class.name // --> java.util.ArrayList

def items = ['Groovy', 'Grails', 'Griffon'] as String[]
println items.size()   // --> 3
println items[-2]      // --> Grails (Read array backwards)
println items.reverse()// --> [Griffon, Grails, Groovy]

println items + ["GPars", "Ratpack"]
    // --> [Groovy, Grails, Griffon, Gradle, GPars, Ratpack]
println items - ["GPars"]
    // --> [Grails, Gradle]

