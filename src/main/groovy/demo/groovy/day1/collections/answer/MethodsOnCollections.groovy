package demo.groovy.day1.collections.answer

import demo.groovy.day1.Person

def list = [
        new Person(firstName: "Steve", lastName: "Johnson", age: 42),
        new Person(firstName: "Eric", lastName: "Peterson", age: 40),
        new Person(firstName: "Eve", lastName: "Andersen", age: 11),
        new Person(firstName: "Douglas", lastName: "Andersen", age: 75),
        new Person(firstName: "Amy", lastName: "Johnson", age: 15),
        new Person(firstName: "Joe", lastName: "Peterson", age: 55),
        new Person(firstName: "Jack", lastName: "Peterson", age: 49),
        new Person(firstName: "Steve", lastName: "Peterson", age: 32),
]

for (Person person : list) {
    System.out.println(person);
}

List result = new ArrayList();
for (Person person : list) {
    result.add(person.getFirstName() + " " + person.getLastName() + " (" + person.getAge() + ")");
}
println result

list.each {
    println it
}



def formatPerson = {
    "$it.firstName $it.lastName ($it.age)"
}

println list.collect(formatPerson)

println list.count { it.lastName == "Peterson" }

println list.sort()

println list.sort {it.firstName }.sort { it.lastName }

println list.sort { a,b ->  a.lastName <=> b.lastName ?: a.firstName <=> b.firstName  }

println list*.age.sum() / list.size()

def byLastName = list.groupBy { it.lastName }
println byLastName

println list.inject(new StringBuffer()) { sb, e ->
    sb.append(e.firstName)
}.toString()

println list*.firstName.sum()
