package demo.groovy.day1.regex

def singleQuotes = ~'[ab] demo \\d' // Escape backslash
def doubleQuotes = ~ "string \$"    // Escape $
def slashy = ~/Slashy\d+value$/     // No escapes needed

println singleQuotes.class.name     // --> java.util.regex.Pattern
println doubleQuotes.class.name     // --> java.util.regex.Pattern
println slashy.class.name           // --> java.util.regex.Pattern


def  pattern = ~/((Gr[\w]+)|(Java))/
String text = """Java is a bit old fashioned, \
 where as Groovy is pretty COOL. \
 Groovy is base of Grails and Griffon"""

def matcher = (text =~ pattern)

println matcher.class.name          // --> java.util.regex.Matcher
while (matcher.find()) {
    println(matcher.group());
}

println(matcher.matches());
println(matcher.matchesPartially());// Extra method on Groovys matcher

matcher.each { println it[0]}       // Each groups first match

println (text ==~ pattern)          // --> false

pattern = ~/(?i).*(Cool).*/
println (text ==~ pattern)          // --> true




println "--"
println (("cheesecheese" =~ "cheese").matches())
println (("cheesecheese" =~ /cheese/).matchesPartially())
println (("chessgame" =~ /cheese/).asBoolean())
