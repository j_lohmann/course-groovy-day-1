package demo.groovy.day1.regex

def technology = "Groovy, Grails, Java, Griffon, Spring, Gradle, Cobol, Gant, Javascript"

def matches = technology =~ /(\w+)/ // Write the correct regex

for(def match: matches) {
    // Prints "Groovy", "Grails", "Griffon" and "Gradle"
    println match[0]
}
assert matches[0][0] == "Groovy"
assert matches[1][0] == "Grails"
assert matches[2][0] == "Griffon"
assert matches[3][0] == "Gradle"

