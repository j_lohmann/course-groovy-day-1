package demo.groovy.day1.gdk

/// Create the file objects
def file1 = new File('groovy1.txt')
def file2 = new File('groovy2.txt')
def file3 = new File('groovy3.txt')

// Write to file
file1.write 'It is super easy to write to a file in Groovy.\n'

// Append to file with leftShift:
file1 << 'And to append to the file.\n'

// Use the text property of a file:
file2.text = '''We can even do tihs in one go
Just look how Groovy this is!'''

// Or we can use a writer to add content,
// and control the encoding
file3.withWriter('UTF-8') { writer ->
   writer.write('We can also use writers to add contents.')
}

// Reading is just as easy
def lines = file1.readLines()
println lines.size() // --> 2
println lines[0]
    // --> It is super easy to write to a file in Groovy.

// Indlæsning vha text property
println file3.text
    // --> We can also use writers to add contents.

// Or use a read with a reader
file2.withReader { reader ->
   while (line = reader.readLine()) {
       println line
   }
}
    // --> We can even do tihs in one go
    // --> Just look how Groovy this is!

// Methods for directory access
files = []
new File('.').eachFileMatch(~/^groovy.*\.txt$/) {
    files << it
}
println files*.name
    // --> [groovy1.txt, groovy2.txt, groovy3.txt]

// Delete the files
files.each { it.delete() }
