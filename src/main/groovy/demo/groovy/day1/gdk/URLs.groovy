package demo.groovy.day1.gdk
// Start to convert a string to a URL
def url = "http://geo.oiorest.dk/regioner.xml".toURL()

// Load the entire text
println url.text

// Load a URL line by line and print content
//url.eachLine { println it }

