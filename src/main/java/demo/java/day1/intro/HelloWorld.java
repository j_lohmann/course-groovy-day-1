package demo.java.day1.intro;

public class HelloWorld {
    public String[] getNames() { return names; }

    public void setNames(String... names) { this.names = names; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (String string : getNames()) {
            sb.append(delimiter).append(string);
            delimiter = ", ";
        }

        return "Hello " + sb.toString();
    }

    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.setNames("Morten", "Marianne", "Donald", "Frank", "Peter");
        System.out.println(helloWorld);
    }

    private String[] names;
}
