package demo.java.day1;

abstract class Human {}
class Male extends Human {}
class Female extends Human {}

public class MultiDispatch {
    static void call(Human p) {
        System.out.println("Call with Human");
    }
    static void call(Male p) {
        System.out.println("Call with Male");
    }
    static void call(Female p) {
        System.out.println("Call with Female");
    }

    public static void main(String[] args) {
        Human p = new Male();
        Human c = new Female();

        call(p);
        call(c);
    }
}

